class proftpd {
  $proftpd_modules_base = ["mod_ctrls_admin.c", 'mod_tls.c', 'mod_radius.c', 'mod_quotatab.c', 'mod_quotatab_file.c', 'mod_quotatab_radius.c',
    'mod_wrap.c', 'mod_rewrite.c', 'mod_load.c', 'mod_ban.c', 'mod_wrap2.c', 'mod_wrap2_file.c', 'mod_dynmasq.c', 'mod_exec.c',
    'mod_shaper.c', 'mod_ratio.c', 'mod_site_misc.c', 'mod_sftp.c', 'mod_sftp_pam.c', 'mod_facl.c', 'mod_unique_id.c',
    'mod_copy.c', 'mod_deflate.c', 'mod_ifversion.c', 'mod_tls_memcache.c', "mod_sql.c", "mod_sql_postgres.c"]

  Package { ensure => 'installed',
  install_options => ['--allow-unauthenticated', '-f'] }

  $proftpd_modules_extra = []

  $ftpd_passwd = $::gce_tag_environment ? {
    'pre'   => 'root-ftp:$1$dT5uctAz$1MZPgG.kTSRA.1cJp03X8/:111:0::/var/ftpgateways:/bin/bash',
    'prod'  => 'root-ftp:$1$WD030gGy$EjyAAoMcWHMXaqg52f.JJ.:115:0::/var/ftpgateways:/bin/bash',
  }

  $proftpd_package = ['proftpd-basic']
  package { $proftpd_package: }
  ->service { 'proftpd':
    ensure => 'stopped',
    enable => false,
  }

  file { '/etc/default/proftpd':
    ensure => file,
    source => "puppet:///modules/${module_name}/default.proftpd",
    owner  => 'root',
    group  => 'root',
    mode   => '0664',
  }
  ->file { '/etc/proftpd/modules.conf':
    ensure  => file,
    content => template("${module_name}/modules_conf.erb"),
    owner   => 'root',
    group   => 'root',
    mode    => '0644',
    notify  => Service['docker-proftpd'],
  }

  file { '/etc/proftpd/ftpd.group':
    ensure => file,
    owner  => 'root',
    group  => 'root',
    mode   => '0440',
    notify => Service['docker-proftpd'],
  }

  file { '/etc/proftpd/ftpd.passwd':
    ensure  => file,
    owner   => 'root',
    group   => 'root',
    mode    => '0440',
    notify  => Service['docker-proftpd'],
    content => $ftpd_passwd,
  }
}
