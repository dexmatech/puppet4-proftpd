class proftpd::sql(
  $proftpd_sqlconnectinfo = undef,
  $proftpd_sqluserinfo = undef,
  $proftpd_sql_uid = undef,
) {

  $proftpd_sql_module_package = [ 'proftpd-mod-pgsql' ]

  package { $proftpd_sql_module_package: }->


  file { '/etc/proftpd/sql.conf':
    ensure  => file,
    content => template("${module_name}/sql_conf.erb"),
    owner   => 'root',
    group   => 'root',
    mode    => '0644',
    notify  => Service['docker-proftpd'],
  }

}
