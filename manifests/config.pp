class proftpd::config(
  $proftpd_passive_ports = undef,
  $proftpd_masquerade_address = undef,
  $proftpd_server_ident = undef,
  $proftpd_max_instances = '150',
  $proftpd_sftp_enable = false
){

  file { '/etc/proftpd/proftpd.conf':
    ensure  => file,
    content => template("${module_name}/proftpd_conf.erb"),
    owner   => 'root',
    group   => 'root',
    mode    => '0644',
    notify  => Service['docker-proftpd'],
  }

}
