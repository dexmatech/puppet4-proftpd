class proftpd::ftps(
	$proftpd_ftps_port = undef,
  $proftpd_ftps_cert = undef,
  $proftpd_ftps_key = undef,
	$proftpd_ftps_masquerade_address = undef,
) {


	file { '/etc/proftpd/ftps.conf':
		ensure  => file,
		content => template("${module_name}/ftps_conf.erb"),
		owner   => 'root',
		group   => 'root',
		mode    => '0644',
		notify  => Service['docker-proftpd'],
	}

}
