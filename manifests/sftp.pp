class proftpd::sftp(
	$proftpd_sftp_port = undef
) {


	file { '/etc/proftpd/sftp.conf':
		ensure  => file,
		content => template("${module_name}/sftp_conf.erb"),
		owner   => 'root',
		group   => 'root',
		mode    => '0644',
		notify  => Service['docker-proftpd'],
	}

}
